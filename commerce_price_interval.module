<?php

/**
 * @file
 */

define('COMMERCE_PRICE_INTERVAL_HORIZONTAL', 0);
define('COMMERCE_PRICE_INTERVAL_VERTICAL', 1);

/**
 * Implements hook_field_info().
 */
function commerce_price_interval_field_info() {
  return array(
    'commerce_price_interval' => array(
      'label' => t('Price interval'),
      'description' => t('This field stores multiple prices for products on an interval.'),
      'settings' => array(),
      'instance_settings' => array(),
      'default_widget' => 'commerce_price_interval_multiple',
      'default_formatter' => 'commerce_multiinterval_default',
      'property_type' => 'commerce_price_interval',
      'property_callbacks' => array('commerce_price_interval_property_info_callback'),
    ),
  );
}

/**
 * Implements hook_field_widget_info().
 */
function commerce_price_interval_field_widget_info() {
  return array(
    'commerce_price_interval_multiple' => array(
      'label' => t('Price interval'),
      'field types' => array('commerce_price_interval'),
      'settings' => array(
        'currency_code' => 'default',
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_info().
 */
function commerce_price_interval_field_formatter_info() {
  return array(
    'commerce_multiinterval_default' => array(
      'label' => t('Price interval chart'),
      'field types' => array('commerce_price_interval'),
      'settings' => array(
        'calculation' => FALSE,
        'price_label' => t('Price'),
        'interval_label' => t('Interval'),
        'table_orientation' => t('Orientation'),
      ),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function commerce_price_interval_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  // Fetch list of intervals.
  $intervals = interval_get_intervals();

  // Convenience variables.
  $value = isset($items[$delta]) ? $items[$delta] : array('interval' => NULL, 'period' => NULL);
  $field_name = $instance['field_name'];

  // We need to check our form_state values for ajax completion.
  if (isset($form_state['values']) &&
    !empty($form_state['values'][$field_name][$langcode][$delta])) {
    // We use the current form state values instead of those from the db.
    $value = $form_state['values'][$field_name][$langcode][$delta];
  }

  $settings = $instance['settings'];
  $required = $element['#required'];
  $widget = $instance['widget'];

  // Available periods.
  $periods = array();
  if (isset($settings['allowed_periods'])) {
    $periods = array_keys(array_filter($settings['allowed_periods']));
  }
  $period_options = array();
  foreach ($intervals as $key => $detail) {
    if (in_array($key, $periods) && isset($detail['plural'])) {
      $period_options[$key] = $detail['plural'];
    }
  }

  // Use the default currency if the setting is not present.
  if (empty($instance['widget']['settings']['currency_code']) || $instance['widget']['settings']['currency_code'] == 'default') {
    $default_currency_code = NULL;
  }
  else {
    $default_currency_code = $instance['widget']['settings']['currency_code'];
  }

  // If a price has already been set for this instance prepare default values.
  if (isset($items[$delta]['amount'])) {
    $currency = commerce_currency_load($items[$delta]['currency_code']);

    // Round the default value.
    $default_amount = commerce_currency_amount_to_decimal($items[$delta]['amount'], $currency['code']);

    // Run it through number_format() to add the decimal places in if necessary.
    if (strpos($default_amount, '.') === FALSE || strpos($default_amount, '.') > strlen($default_amount) - $currency['decimals']) {
      $default_amount = number_format($default_amount, $currency['decimals'], '.', '');
    }

    $default_currency_code = $items[$delta]['currency_code'];
  }
  else {
    $default_amount = NULL;
  }

  // Load the default currency for this instance.
  $default_currency = commerce_currency_load($default_currency_code);

  $element['#attached']['css'][] = drupal_get_path('module', 'commerce_price_interval') . '/theme/commerce_price_interval.css';

  if ($instance['widget']['type'] == 'commerce_price_interval_multiple') {
    $element['amount'] = array(
      '#type' => 'textfield',
      '#title' => $element['#title'],
      '#description' => t('Amount for the price.'),
      '#default_value' => $default_amount,
      '#size' => 10,
      '#field_suffix' => $default_currency['code'],
    );
    $element['currency_code'] = array(
      '#type' => 'value',
      '#default_value' => $default_currency['code'],
    );
  }
  $element['interval'] = array(
    '#type' => 'textfield',
    '#size' => 8,
    '#required' => $required,
    '#attributes' => array('class' => array('interval-interval')),
    '#default_value' => $value['interval']
  );
  $element['period'] = array(
    '#type' => 'select',
    '#options' => $period_options,
    '#attributes' => array('class' => array('interval-period')),
    '#default_value' => $value['period']
  );

  $element['data'] = array(
    '#type' => 'value',
    '#default_value' => !empty($items[$delta]['data']) ? $items[$delta]['data'] : array('components' => array()),
  );

  $element['#element_validate'][] = 'commerce_price_interval_field_widget_validate';

  return $element;
}

/**
 * Validate callback: ensures the amount value is numeric and converts it from a
 * decimal value to an integer price amount.
 */
function commerce_price_interval_field_widget_validate($element, &$form_state) {
  if ($element['amount']['#value'] !== '') {
    // Ensure the price is numeric.
    if (!is_numeric($element['amount']['#value'])) {
      form_error($element['amount'], t('%title: you must enter a numeric value for the price amount.', array('%title' => $element['amount']['#title'])));
    }
    else {
      // Convert the decimal amount value entered to an integer based amount value.
      form_set_value($element['amount'], commerce_currency_decimal_to_amount($element['amount']['#value'], $element['currency_code']['#value']), $form_state);
    }
  }
}

/**
 * Implements hook_field_presave().
 */
function commerce_price_interval_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {
  // Convert amounts to integers and serialize data arrays before saving.
  foreach ($items as $delta => $item) {
    // Serialize an existing data array.
    if (isset($item['data']) && is_array($item['data'])) {
      $items[$delta]['data'] = serialize($item['data']);
    }
  }
}

/**
 * Implements hook_field_load().
 */
function commerce_price_interval_field_load($entity_type, $entities, $field, $instances, $langcode, &$items, $age) {
  // Convert amounts to their floating point values and deserialize data arrays.
  foreach ($entities as $id => $entity) {
    foreach ($items[$id] as $delta => $item) {
      // Unserialize the data array if necessary.
      if (!empty($items[$id][$delta]['data'])) {
        $items[$id][$delta]['data'] = unserialize($items[$id][$delta]['data']);
      }
      else {
        $items[$id][$delta]['data'] = array('components' => array());
      }
    }
  }
}

/**
 * Implements hook_field_validate().
 */
function commerce_price_interval_field_validate($entity_type, $entity, $field, $instance, $langcode, &$items, &$errors) {
  // Ensure only numeric values are entered in price fields.
  foreach ($items as $delta => $item) {
    if (!empty($item['amount']) && !is_numeric($item['amount'])) {
      $errors[$field['field_name']][$langcode][$delta][] = array(
        'error' => 'price_numeric',
        'message' => t('%name: you must enter a numeric value for the price.', array('%name' => check_plain($instance['label']))),
      );
    }
    if (!empty($item['interval'])) {
      if (!is_numeric($item['interval'])) {
        $errors[$field['field_name']][$langcode][$delta][] = array(
          'error' => 'interval_non_numeric',
          'message' => t('You must enter a numeric value.'),
        );
      }
    }
  }
}

/**
 * Implements hook_field_is_empty().
 */
function commerce_price_interval_field_is_empty($item, $field) {
  return (!isset($item['amount']) || (string) $item['amount'] == '' || empty($item['interval']));
}

/**
 * Implements hook_field_widget_error().
 */
function commerce_price_interval_field_widget_error($element, $error, $form, &$form_state) {
  switch ($error['error']) {
    case 'price_numeric':
    case 'interval_non_numeric':
      form_error($element, $error['message']);
      break;
  }
}

/**
 * Callback to alter the property info of price fields.
 *
 * @see commerce_price_field_info().
 */
function commerce_price_interval_property_info_callback(&$info, $entity_type, $field, $instance, $field_type) {
  $name = $field['field_name'];
  $property = &$info[$entity_type]['bundles'][$instance['bundle']]['properties'][$name];

  $property['type'] = ($field['cardinality'] != 1) ? 'list<commerce_price_interval>' : 'commerce_price_interval';
  $property['getter callback'] = 'entity_metadata_field_verbatim_get';
  $property['setter callback'] = 'entity_metadata_field_verbatim_set';
  $property['auto creation'] = 'commerce_price_field_data_auto_creation';
  $property['property info'] = commerce_price_interval_field_data_property_info();

  unset($property['query callback']);
}

/**
 * Implements hook_field_instance_settings_form().
 */
function commerce_price_interval_field_instance_settings_form($field, $instance) {
  $form = array();
  $settings = $instance['settings'];
  $form['commerce_price_interval'] = array(
    '#type' => 'fieldset',
    '#title' => t('Commerce price table settings'),
  );
  $widget = $instance['widget']['type'];
  $options = array();
  $intervals = interval_get_intervals();
  foreach ($intervals as $key => $detail) {
    $options[$key] = $detail['plural'];
  }
  $form['allowed_periods'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Allowed periods'),
    '#options' => $options,
    '#options' => $options,
    '#description' => t('Select the periods you wish to be available in the dropdown. Selecting none will make all of them available.'),
    '#default_value' => isset($settings['allowed_periods']) ? $settings['allowed_periods'] : array()
  );
  return $form;
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function commerce_price_interval_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $element = array();

  if ($display['type'] == 'commerce_multiinterval_default') {
    $element['price_label'] = array(
      '#type' => 'textfield',
      '#title' => t('Price label for the price table'),
      '#default_value' => isset($settings['price_label']) ? $settings['price_label'] : t('Price'),
    );
    $element['interval_label'] = array(
      '#type' => 'textfield',
      '#title' => t('Interval label'),
      '#default_value' => isset($settings['interval_label']) ? $settings['interval_label'] : t('Interval'),
    );
    $element['table_orientation'] = array(
      '#type' => 'radios',
      '#options' => array(
        COMMERCE_PRICE_INTERVAL_HORIZONTAL => t('Horizontal'),
        COMMERCE_PRICE_INTERVAL_VERTICAL => t('Vertical'),
      ),
      '#title' => t('Orientation of the price table'),
      '#default_value' => isset($settings['table_orientation']) ? $settings['table_orientation'] : COMMERCE_PRICE_INTERVAL_HORIZONTAL,
    );
  }

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function commerce_price_interval_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $summary = array();

  if ($display['type'] == 'commerce_multiinterval_default') {
    $orientation = isset($settings['table_orientation']) && $settings['table_orientation'] == COMMERCE_PRICE_INTERVAL_VERTICAL ? t('Vertical') : t('Horizontal');
    $summary = array(
      t('Interval label: !interval_label', array('!interval_label' => isset($settings['interval_label']) ? $settings['interval_label'] : t('Interval'))),
      t('Price label: !price_label', array('!price_label' => isset($settings['price_label']) ? $settings['price_label'] : t('Price'))),
      t('Orientation: !orientation', array('!orientation' => $orientation)),
    );
  }

  return implode('<br />', $summary);
}

/**
 * Defines info for the properties of the Price field data structure.
 */
function commerce_price_interval_field_data_property_info($name = NULL) {
  return array(
    'amount' => array(
      'label' => t('Amount'),
      'description' => !empty($name) ? t('Amount value of field %name', array('%name' => $name)) : '',
      'type' => 'decimal',
      'getter callback' => 'entity_property_verbatim_get',
      'setter callback' => 'entity_property_verbatim_set',
    ),
    'currency_code' => array(
      'label' => t('Currency'),
      'description' => !empty($name) ? t('Currency code of field %name', array('%name' => $name)) : '',
      'type' => 'text',
      'getter callback' => 'entity_property_verbatim_get',
      'setter callback' => 'entity_property_verbatim_set',
      'options list' => 'commerce_currency_code_options_list',
    ),
    'interval' => array(
      'type' => 'integer',
      'label' => t('Interval number'),
      'description' => t('The number of multiples of the period.'),
    ),
    'period' => array(
      'type' => 'token',
      'label' => t('Interval period'),
      'options list' => 'interval_period_options_list',
    ),
    'data' => array(
      'label' => t('Data'),
      'description' => !empty($name) ? t('Data array of field %name', array('%name' => $name)) : '',
      'type' => 'struct',
      'getter callback' => 'entity_property_verbatim_get',
      'setter callback' => 'entity_property_verbatim_set',
    ),
  );
}

/**
 * Implements hook_theme().
 */
function commerce_price_interval_theme() {
  return array(
    'commerce_multiinterval_default' => array(
      'variables' => array('items' => array()),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function commerce_price_interval_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  if ($display['type'] == 'commerce_multiinterval_default' && !empty($items)) {
    $header = array(isset($display['settings']['interval_label']) ? $display['settings']['interval_label'] : t('Interval'));
    $row = array(isset($display['settings']['price_label']) ? $display['settings']['price_label'] : t('Price'));
    // Not a product. We're dealing with exotic stuff here.
    foreach ($items as $delta => $item) {
      $header[] = commerce_price_interval_display_interval_headers($item);
      $row[] = array('data' => commerce_currency_format($item['amount'], $item['currency_code'], $entity));
    }
  }

  // By default, the price table is rendered horizontally. If vertical
  // orientation was chosen, flip the table.
  if (isset($display['settings']['table_orientation']) && $display['settings']['table_orientation'] == COMMERCE_PRICE_INTERVAL_VERTICAL) {
    $rows = array();
    $header_old = $header;

    $header = array($header_old[0], $row[0]);
    for ($index = 1; $index < count($row); $index++) {
      $rows[] = array('data' => array($header_old[$index], $row[$index]['data']));
    }
  }
  else {
    $rows = array($row);
  }

  $element[] = array(
    '#markup' => theme('table', array('header' => $header, 'rows' => $rows)),
  );

  return $element;
}

/**
 * Return the settings of all the price table field of a bundle.
 */
function commerce_price_interval_get_field_instance_settings($entity_type = 'commerce_product', $bundle = 'product') {
  $settings = array();
  $fields = commerce_info_fields('commerce_price_interval', $entity_type);
  if (isset($fields) && is_array($fields)) {
    foreach ($fields as $field) {
      $settings[] = field_info_instance($entity_type, $field['field_name'], $bundle);
    }
  }

  return $settings;
}

/**
 * Helper function that takes care of the quantity displayed in the headers of
 * the price table.
 */
function commerce_price_interval_display_interval_headers($item) {
  return interval_format_interval($item);
}
