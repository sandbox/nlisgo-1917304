Drupal Commerce contrib project that allows to have a secondary price field that considers the interval period for displaying and using the prices. It also provides a field formatter to display the price/interval values as a table.

The use case for this module is to provide a way of offering different subscription rates for a product based upon the interval period of payment (e.g. cost for 1 month, cost for 1 year).

Instructions
* Enable the module
* Create a new field in the product bundle of the type 'Price interval', with unlimited or a fixed set of values.
* Add/edit your products and add to them as many prices as you want and the corresponding interval periods.
* You can display the price interval table in horizontal or vertical just changing the formatter setting.